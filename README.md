# Drupal 8, Search API and Solr 8

## Drupal composer install

### Use current repository as main project example.

[Recommended] You can use current project.

```bash
git clone git@bitbucket.org:metadrop/drupal-8-search-api-solr-8.git
composer install
```

### Or you can create your current site using _Composer template for Drupal projects_

```bash
composer create-project drupal-composer/drupal-project:8.x-dev some-dir --stability dev --no-interaction
```

With `composer require ...` you can download new dependencies to your
installation.

```bash
cd some-dir
composer require drupal/search_api_solr:~3.0
```

## Start docker.

```bash
cp .env.example .env
docker-compose up -d
```

### Ahoy Cli

This project can run with [Ahoy Cli](http://www.ahoycli.com/en/latest/) commands.

## Configure solr.

```bash
docker-compose exec solr bash
sh /var/www/html/bin/configure-solr-docker.sh
```
Validate if working properly on: [Solr](http://solr.drupalsolr.docker.localhost:8000/solr/#/~cores/d8)

## Install drupal.

```bash
docker-compose exec php bash
drush si --db-url=mysql://drupal:drupal@drupalsolr_mariadb/drupal --db-su=root --db-su-pw=password --account-name=root --account-pass=root -y
drush en search_api_solr -y
drush en search_api_solr_defaults -y
drush uli --uri=http://drupalsolr.docker.localhost:8000
```

### Create demo content
```bash
docker-compose exec php bash
drupal create:nodes article --limit="10" --title-words="5" --time-range="1" --language="en" --no-interaction
```
## Configure and run indexation
Now configure host correctly:

### Solr server setup
- Go to _Solr server_ setup: [admin/config/search/search-api/server/default_solr_server/edit](http://drupalsolr.docker.localhost:8000/admin/config/search/search-api/server/default_solr_server/edit)
- Change on _Solr host_ from _localhost_ to _solr_
- Save.

### Configure default index
- Go to _Default Solr content index_ [admin/config/search/search-api/index/default_solr_index/edit](http://drupalsolr.docker.localhost:8000/admin/config/search/search-api/index/default_solr_index/edit)
- Create content before run.

### Run index
- Run index: [admin/config/search/search-api/index/default_solr_index](http://drupalsolr.docker.localhost:8000/admin/config/search/search-api/index/default_solr_index)

## Test Solr search

- Go to: [/solr-search/content](http://drupalsolr.docker.localhost:8000/solr-search/content)
- Try.
